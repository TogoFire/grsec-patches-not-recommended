# nu11secur1ty-grsec-patches

# Installing and deploying
- 1 .Download your kernel from [Linux Kernel](http://ftp.funet.fi/pub/linux/kernel/) and prepare for patching and compiling
```bash
tar -xvf linux-4.9.x.tar.gz
cd linux-4.9.x
```
- 2 .Patching the Linux Kernel
```bash
cd linux-4.9.x
patch -p1 < ../nu11secur1ty-grsec-patches/grsec-4.9/grsecurity-3.1-4.9.24-201704252333.patch
```
 - In my case the release is `4.9.24`

- 3 .Compiling the Kernel
```bash
cd linux-4.9.x
make mrproper
make rpm-pkg    # If you already prepire your machine for Linux Kernel Developement
```

# Good luck of everyone ;) 
- BR nu11secur1ty



